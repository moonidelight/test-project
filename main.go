package main

import (
	"fmt"
	"github.com/go-git/go-git/v5/config"
	"github.com/joho/godotenv"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"log"
	"os"
	"time"

	git2 "github.com/go-git/go-git/v5"
	"gopkg.in/src-d/go-git.v4"
	. "gopkg.in/src-d/go-git.v4/_examples"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
)

func main() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal(err)
	}

	//file := "html/locales/en/translation.json"
	file := "test1/translation.json"
	commitMsg := "updated file"
	targetBranch := "test"

	//directory := "stars"
	directory := "test"
	text := []byte(`{app: "smth"}`)
	err = os.WriteFile(directory+"/"+file, text, 0644)
	CheckIfError(err)
	// Opens an already existing repository1.
	r, err := git.PlainOpen(directory)
	CheckIfError(err)

	w, err := r.Worktree()
	CheckIfError(err)

	Info(fmt.Sprintf("git add %s", file))
	_, err = w.Add(file)
	CheckIfError(err)

	Info("git status --porcelain")
	status, err := w.Status()
	CheckIfError(err)

	fmt.Println(status)

	Info("git commit -m \"text\"")
	commit, err := w.Commit(commitMsg, &git.CommitOptions{
		Author: &object.Signature{
			Name:  os.Getenv("GitName"),
			Email: os.Getenv("GitEmail"),
			When:  time.Now(),
		},
	})
	CheckIfError(err)
	err = w.Checkout(&git.CheckoutOptions{
		Branch: plumbing.ReferenceName("refs/heads/" + targetBranch),
	})
	CheckIfError(err)
	// Prints the current HEAD to verify that all worked well.
	Info("git show -s")
	obj, err := r.CommitObject(commit)
	CheckIfError(err)
	fmt.Println(obj)

	r1, err := git2.PlainOpen(directory)
	CheckIfError(err)
	Info("git push")
	remoteUrl := fmt.Sprintf(`https://%s:%s@%s`, os.Getenv("GitUsername"), os.Getenv("GitPassword"), os.Getenv("GitUrl"))
	err = r1.Push(&git2.PushOptions{
		RemoteName: "origin",
		RemoteURL:  remoteUrl,
		RefSpecs: []config.RefSpec{
			config.RefSpec(fmt.Sprintf("refs/heads/%s:refs/heads/%s", targetBranch, targetBranch)),
		},
	})
	CheckIfError(err)
}
